package sheridan;

//Cory Salmon
public class Fahrenheit {
	static int MAX_DEGREES_C = 1193046452;
	
	public static int fromCelsius(int input) {
		if (input > MAX_DEGREES_C) {
			throw new ArithmeticException("Out of range");
		}
		return (int)Math.round(input * 9.0/5.0 +32.0);
	}
}
