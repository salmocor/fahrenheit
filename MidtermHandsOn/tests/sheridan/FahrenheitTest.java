package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class FahrenheitTest {

	@Test
	public void testFromCelsiusRegular() {
		int inputValue = 10;
		int expectedValue = 50;
		assertEquals("Temperature conversion was not valid", expectedValue, Fahrenheit.fromCelsius(inputValue));
	}
	
	@Test (expected = ArithmeticException.class)
	public void testFromCelsiusException() {
		int inputValue = Integer.MAX_VALUE;
		Fahrenheit.fromCelsius(inputValue);
	}
	
	@Test
	public void testFromCelsiusBoundaryIn() {
		int inputValue = 1193046452;
		int expectedValue = 2147483646;
		assertEquals("Temperature conversion was not valid", expectedValue, Fahrenheit.fromCelsius(inputValue));
	}
	
	@Test (expected = ArithmeticException.class)
	public void testFromCelsiusBoundaryOut() {
		int inputValue = 1193046453;
		Fahrenheit.fromCelsius(inputValue);
	}

}
